document.addEventListener("DOMContentLoaded", function(event) {
    var thumbnailElement = document.getElementById("smart_thumbnail");
    var emojiElement = document.getElementById("smart_emoji");
    thumbnailElement.addEventListener("click", function() {
        if (thumbnailElement.className == "") {
            emojiElement.innerText = "😄";
            thumbnailElement.className = "small";
        } else {
            thumbnailElement.className = "";
            emojiElement.innerText = "😅";
        }
      });
});
